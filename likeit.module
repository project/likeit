<?php

/**
 * @file
 * Contains Likeit module helper functions.
 */

use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\user\UserInterface;
use Drupal\views\Plugin\views\PluginBase;
use Drupal\views\ViewExecutable;

/**
 * Implements hook_help().
 */
function likeit_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'help.page.likeit':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Provides link for user to like the entity.') . '</p>';
      return $output;
  }
}

/**
 * Implements hook_entity_extra_field_info().
 */
function likeit_entity_extra_field_info() {
  $extra = [];

  $config = \Drupal::config('likeit.settings');
  $target_entities = $config->get('target_entities');

  if (!empty($target_entities)) {
    foreach ($target_entities as $key => $entity) {
      $target_arr = explode(':', $key);
      $entity_type = $target_arr[0];
      $entity_bundle = $target_arr[1];
      $extra[$entity_type][$entity_bundle]['display']['likeit'] = [
        'label' => t('Like It!'),
        'description' => t('Like and Unlike Link.'),
        'weight' => 0,
        'visible' => FALSE,
        'render_class' => 'Drupal\likeit\LikeItExtraRender',
      ];
    }
  }

  return $extra;
}

/**
 * Implements hook_entity_view().
 */
function likeit_entity_view(array &$build, EntityInterface $entity, EntityViewDisplayInterface $display, $view_mode) {
  // Don't show on previews.
  if ($entity->isNew()) {
    return;
  }
  $config = \Drupal::config('likeit.settings');
  $target_entities = $config->get('target_entities');

  if (!empty($target_entities) && $display->getComponent('likeit')) {
    $target = $entity->getEntityTypeId() . ':' . $entity->bundle();
    if (in_array($target, $target_entities)) {
      $build['likeit'] = \Drupal::service('likeit.helper')->getLink($target, $entity->id());
    }
  }
}

/**
 * Implements hook_theme().
 */
function likeit_theme($existing, $type, $theme, $path) {
  return [
    'likeit' => [
      'variables' => [
        'content' => [
          'link' => NULL,
          'view' => NULL,
          'message' => NULL,
        ],
        'action' => '',
        'count' => '',
        'attributes' => [
          'class' => ['likeit', 'likeit-wrapper'],
        ],
      ],
    ],
  ];
}

/**
 * Get link to like/unlike or count info.
 *
 * @param string $target
 *   Target entity:bundle.
 * @param string $id
 *   Target bundle id.
 *
 * @return array
 *   Render or empty array.
 *
 * @deprecated in likeit:2.2.0 and is removed from likeit:3.0.0. Use
 *   \Drupal\likeit\LikeitHelper::getlink() instead.
 */
function likeit_get_link($target, $id) {
  @trigger_error(__FUNCTION__ . '() is deprecated in likeit:2.2.0 and is removed from likeit:3.0.0. Use \Drupal\likeit\LikeitHelper::getlink() instead. See https://www.drupal.org/project/likeit/issues/3346020', E_USER_DEPRECATED);

  return \Drupal::service('likeit.helper')->getLink($target, $id);
}

/**
 * Get likes count.
 *
 * @param object $entity
 *   Target entity.
 *
 * @return int
 *   Likes count for the entity.
 *
 * @deprecated in likeit:2.2.0 and is removed from likeit:3.0.0. Use
 *   \Drupal\likeit\LikeitHelper::getCount() instead.
 */
function likeit_get_count($entity) {
  @trigger_error(__FUNCTION__ . '() is deprecated in likeit:2.2.0 and is removed from likeit:3.0.0. Use \Drupal\likeit\LikeitHelper::getCount() instead. See https://www.drupal.org/project/likeit/issues/3346020', E_USER_DEPRECATED);

  return \Drupal::service('likeit.helper')->getCount($entity);
}

/**
 * Build content likes view.
 *
 * @param string $target
 *   Target entity:bundle.
 * @param string $id
 *   Target entity id.
 *
 * @return array
 *   Render or empty array.
 *
 * @deprecated in likeit:2.2.0 and is removed from likeit:3.0.0. Use
 *   \Drupal\likeit\LikeitHelper::buildView() instead.
 */
function likeit_build_view($target, $id) {
  @trigger_error(__FUNCTION__ . '() is deprecated in likeit:2.2.0 and is removed from likeit:3.0.0. Use \Drupal\likeit\LikeitHelper::buildView() instead. See https://www.drupal.org/project/likeit/issues/3346020', E_USER_DEPRECATED);

  return \Drupal::service('likeit.helper')->buildView($target, $id);
}

/**
 * Build the link widget.
 *
 * @param string $target
 *   Target entity:bundle.
 * @param string $id
 *   Target entity id.
 * @param string $action
 *   (optional) Action name.
 *
 * @return array
 *   Like/unlike render array.
 *
 * @deprecated in likeit:2.2.0 and is removed from likeit:3.0.0. Use
 *   \Drupal\likeit\LikeitHelper::buildLink() instead.
 */
function likeit_build_link($target, $id, $action = 'like') {
  @trigger_error(__FUNCTION__ . '() is deprecated in likeit:2.2.0 and is removed from likeit:3.0.0. Use \Drupal\likeit\LikeitHelper::buildLink() instead. See https://www.drupal.org/project/likeit/issues/3346020', E_USER_DEPRECATED);

  return \Drupal::service('likeit.helper')->buildLink($target, $id, $action);
}

/**
 * Get user session ID.
 *
 * @return string
 *   Session id.
 *
 * @deprecated in likeit:2.2.0 and is removed from likeit:3.0.0.
 */
function likeit_get_user_session_id() {
  @trigger_error(__FUNCTION__ . '() is deprecated in likeit:2.2.0 and is removed from likeit:3.0.0. See https://www.drupal.org/project/likeit/issues/3346020', E_USER_DEPRECATED);

  return \Drupal::service('likeit.helper')->getUserSessionId();
}

/**
 * Do like.
 *
 * @param \Drupal\Core\Entity\EntityInterface $entity
 *   Target entity.
 * @param \Drupal\Core\Session\AccountInterface|null $account
 *   (optional) User account.
 *
 * @return string|null
 *   Session id or null.
 *
 * @deprecated in likeit:2.2.0 and is removed from likeit:3.0.0. Use
 *   \Drupal\likeit\LikeitHelper::doLike() instead.
 */
function likeit_do_like(EntityInterface $entity, AccountInterface $account = NULL) {
  @trigger_error(__FUNCTION__ . '() is deprecated in likeit:2.2.0 and is removed from likeit:3.0.0. Use \Drupal\likeit\LikeitHelper::doLike() instead. See https://www.drupal.org/project/likeit/issues/3346020', E_USER_DEPRECATED);

  return \Drupal::service('likeit.helper')->doLike($entity, $account);
}

/**
 * Do unlike.
 *
 * @param \Drupal\Core\Entity\EntityInterface $entity
 *   Target entity.
 * @param \Drupal\Core\Session\AccountInterface|null $account
 *   (optional) User account.
 *
 * @return string|null
 *   Session id or null.
 *
 * @deprecated in likeit:2.2.0 and is removed from likeit:3.0.0. Use
 *   \Drupal\likeit\LikeitHelper::doUnlike() instead.
 */
function likeit_do_unlike(EntityInterface $entity, AccountInterface $account = NULL) {
  @trigger_error(__FUNCTION__ . '() is deprecated in likeit:2.2.0 and is removed from likeit:3.0.0. Use \Drupal\likeit\LikeitHelper::doUnlike() instead. See https://www.drupal.org/project/likeit/issues/3346020', E_USER_DEPRECATED);

  return \Drupal::service('likeit.helper')->doUnlike($entity, $account);
}

/**
 * Set cookie.
 *
 * @param string $session_id
 *   User session id.
 *
 * @deprecated in likeit:2.2.0 and is removed from likeit:3.0.0.
 */
function likeit_set_cookie($session_id) {
  @trigger_error(__FUNCTION__ . '() is deprecated in likeit:2.2.0 and is removed from likeit:3.0.0. See https://www.drupal.org/project/likeit/issues/3346020', E_USER_DEPRECATED);

  \Drupal::service('likeit.helper')->setCookie($session_id);
}

/**
 * Get cookie.
 *
 * @return string
 *   Cookie id.
 *
 * @deprecated in likeit:2.2.0 and is removed from likeit:3.0.0.
 */
function likeit_get_cookie() {
  @trigger_error(__FUNCTION__ . '() is deprecated in likeit:2.2.0 and is removed from likeit:3.0.0. See https://www.drupal.org/project/likeit/issues/3346020', E_USER_DEPRECATED);

  return \Drupal::service('likeit.helper')->getCookie();
}

/**
 * Get link action type.
 *
 * @param string $target
 *   Target entity:bundle.
 * @param string $id
 *   Target entity id.
 *
 * @return string|null
 *   Action name or null.
 *
 * @deprecated in likeit:2.2.0 and is removed from likeit:3.0.0.
 */
function likeit_get_link_action_type($target, $id) {
  @trigger_error(__FUNCTION__ . '() is deprecated in likeit:2.2.0 and is removed from likeit:3.0.0. See https://www.drupal.org/project/likeit/issues/3346020', E_USER_DEPRECATED);

  return \Drupal::service('likeit.helper')->getLinkActionType($target, $id);
}

/**
 * Check like status.
 *
 * @param object $entity
 *   Target entity.
 * @param \Drupal\Core\Session\AccountInterface|null $user
 *   (optional) User account.
 *
 * @return bool
 *   User like status. TRUE if entity was already liked.
 *
 * @deprecated in likeit:2.2.0 and is removed from likeit:3.0.0. Use
 *   \Drupal\likeit\LikeitHelper::statusCheck() instead.
 */
function likeit_check($entity, AccountInterface $user = NULL) {
  @trigger_error(__FUNCTION__ . '() is deprecated in likeit:2.2.0 and is removed from likeit:3.0.0. Use \Drupal\likeit\LikeitHelper::statusCheck() instead. See https://www.drupal.org/project/likeit/issues/3346020', E_USER_DEPRECATED);

  return \Drupal::service('likeit.helper')->statusCheck($entity, $user);
}

/**
 * Implements hook_entity_delete().
 */
function likeit_entity_delete(EntityInterface $entity) {
  $type = $entity->getEntityTypeId();
  if ($type !== 'likeit') {
    $id = $entity->id();

    // Remove Likeit entity content.
    \Drupal::service('likeit.helper')->removeFromEntity($type, $id);
  }
}

/**
 * Implements hook_user_cancel().
 */
function likeit_user_cancel($edit, $account, $method) {
  \Drupal::service('likeit.helper')->removeFromUser($account);
}

/**
 * Implements hook_user_delete().
 */
function likeit_user_delete(UserInterface $account) {
  \Drupal::service('likeit.helper')->removeFromUser($account);
}

/**
 * Remove all Likeit entities from user account.
 *
 * @param \Drupal\user\UserInterface $account
 *   User account.
 *
 * @deprecated in likeit:2.2.0 and is removed from likeit:3.0.0. Use
 *   \Drupal\likeit\LikeitHelper::removeFromUser() instead.
 */
function likeit_remove_from_user(UserInterface $account) {
  @trigger_error(__FUNCTION__ . '() is deprecated in likeit:2.2.0 and is removed from likeit:3.0.0. Use \Drupal\likeit\LikeitHelper::removeFromUser() instead. See https://www.drupal.org/project/likeit/issues/3346020', E_USER_DEPRECATED);

  \Drupal::service('likeit.helper')->removeFromUser($account);
}

/**
 * Delete Likeit entities with target entity deletion.
 *
 * @param string $type
 *   Target entity type.
 * @param string $id
 *   Target entity id.
 *
 * @deprecated in likeit:2.2.0 and is removed from likeit:3.0.0. Use
 *   \Drupal\likeit\LikeitHelper::removeFromEntity() instead.
 */
function likeit_remove_from_entity($type, $id) {
  @trigger_error(__FUNCTION__ . '() is deprecated in likeit:2.2.0 and is removed from likeit:3.0.0. Use \Drupal\likeit\LikeitHelper::removeFromEntity() instead. See https://www.drupal.org/project/likeit/issues/3346020', E_USER_DEPRECATED);

  \Drupal::service('likeit.helper')->removeFromEntity($type, $id);
}

/**
 * Implements hook_views_query_substitutions().
 */
function likeit_views_query_substitutions(ViewExecutable $view) {
  return [
    '***CURRENT_SESSION_ID***' => \Drupal::service('likeit.helper')->getCookie(),
  ] + PluginBase::queryLanguageSubstitutions();
}
